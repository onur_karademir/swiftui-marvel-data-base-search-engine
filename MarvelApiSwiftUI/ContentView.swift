//
//  ContentView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI
import Firebase

struct ContentView: View {
    @State var isLaunchScreen : Bool = true
    @State var show = false
    @State var status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
    var body: some View {
        NavigationView {
            VStack {
                if isLaunchScreen {
                    LaunchView()
                        .opacity(isLaunchScreen ? 1 : 0)
                        .onAppear {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                withAnimation(.easeInOut(duration: 0.8)) {
                                self.isLaunchScreen = false
                              }
                            }
                        }
                        
                } else {
                    if self.status {
                        MainTabView()
                    } else {
                        ZStack{
                            
                            NavigationLink(destination: SignUp(show: self.$show), isActive: self.$show) {
                                
                                Text("")
                            }
                            .hidden()
                            
                            LoginView(show: self.$show)
                        }

                    }
                }
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .onAppear {
                
                NotificationCenter.default.addObserver(forName: NSNotification.Name("status"), object: nil, queue: .main) { (_) in
                    
                    self.status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
