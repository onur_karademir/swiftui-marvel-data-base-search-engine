//
//  MarvelApiSwiftUIApp.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI
import Firebase

@main
struct MarvelApiSwiftUIApp: App {
    init() {
        
        FirebaseApp.configure()
        
    }
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
