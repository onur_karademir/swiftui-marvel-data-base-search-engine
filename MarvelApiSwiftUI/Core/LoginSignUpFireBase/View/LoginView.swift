//
//  LoginView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 20.10.2022.
//

import SwiftUI
import Firebase

struct LoginView: View {
    
    @State var color = Color.black.opacity(0.7)
    @State var email = ""
    @State var pass = ""
    @State var visible = false
    @Binding var show : Bool
    @State var alert = false
    @State var error = ""
    
    var body: some View {
        
        ZStack {
            
            VStack {
                
                HStack {
                    Spacer()
                    Button {
                        self.show.toggle()
                    } label: {
                        Text("Register")
                            .fontWeight(.bold)
                            .foregroundColor(.pink)
                    }
                }
                .padding(.horizontal)
                
                VStack {
                    
                    GeometryReader {_ in
                        
                        VStack {
                            
                            MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
                                
                            LoginSubTitleView(subTitle: "Log in to your account", color: self.color)
                            
                            TextField("Email", text: self.$email)
                            .autocapitalization(.none)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.email != "" ? .pink : self.color,lineWidth: 2))
                            .padding(.top, 25)
                            
                            HStack(spacing: 15) {
                                
                                VStack {
                                    
                                    if self.visible{
                                        
                                        TextField("Password", text: self.$pass)
                                        .autocapitalization(.none)
                                    }
                                    else{
                                        
                                        SecureField("Password", text: self.$pass)
                                        .autocapitalization(.none)
                                    }
                                }
                                // button password visible or isVisible //
                                Button {
                                    self.visible.toggle()
                                } label: {
                                    Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                                        .foregroundColor(self.color)
                                }
                            }
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.pass != "" ? .pink : self.color,lineWidth: 2))
                            .padding(.top, 25)
                            
                            HStack {
                                
                                Spacer()
                                // reset password button //
                                Button {
                                    self.reset()
                                } label: {
                                    Text("Forget password")
                                        .fontWeight(.bold)
                                        .foregroundColor(.pink)
                                }
                                .padding(.top, 20)
                            }
                            // Login Button //
                            Button {
                                self.verify()
                            } label: {
                               CustomButtonView(buttonText: "Log in", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .pink)
                            }
                            
                            .padding(.top, 25)
                        }
                        .padding(.horizontal, 25)
                    }
                }
                
                
            }
            if self.alert{
                ErrorView(alert: self.$alert, error: self.$error)
            }
        }
    }
    
    func verify() {
        
        if self.email != "" && self.pass != "" {
            
            Auth.auth().signIn(withEmail: self.email, password: self.pass) { (res, err) in
                
                if err != nil {
                    
                    self.error = err!.localizedDescription
                    self.alert.toggle()
                    return
                }
                
                if let user = Auth.auth().currentUser {
                    if user.isEmailVerified == true {
                        print("success")
                        UserDefaults.standard.set(true, forKey: "status")
                        NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
                    } else {
                        self.error = "E-mail address is not verified."
                        self.alert.toggle()
                    }
                }
                
                
            }
        }
        else {
            
            self.error = "Please fill all the contents properly"
            self.alert.toggle()
        }
    }
    
    func reset() {
        
        if self.email != "" {
            
            Auth.auth().sendPasswordReset(withEmail: self.email) { (err) in
                
                if err != nil {
                    
                    self.error = err!.localizedDescription
                    self.alert.toggle()
                    return
                }
                
                self.error = "RESET"
                self.alert.toggle()
            }
        }
        else{
            
            self.error = "Email Id is empty"
            self.alert.toggle()
        }
    }

}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(show: .constant(false))
        //SignUp(show: .constant(false))
    }
}


struct SignUp : View {
    
    @State var color = Color.black.opacity(0.7)
    @State var email = ""
    @State var pass = ""
    @State var repass = ""
    @State var visible = false
    @State var revisible = false
    @Binding var show : Bool
    @State var alert = false
    @State var error = ""
    
    var body: some View{
        
        ZStack {
            
            VStack {
                
                HStack {
                    Button {
                        self.show.toggle()
                    } label: {
                        NavBackButtonView(image: "chevron.left", font: .title, color: .pink)
                    }
                    .padding(10)
                    Spacer()
                }
                .padding(.horizontal)
                
                VStack {
                    
                    GeometryReader {_ in
                        
                        VStack {
                            
                            MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
                            
                        ScrollView (showsIndicators: false) {
                            LoginSubTitleView(subTitle: "Log in to your account", color: self.color)
                            
                            TextField("Email", text: self.$email)
                            .autocapitalization(.none)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.email != "" ? .pink : self.color,lineWidth: 2))
                            .padding(.top, 25)
                            
                            HStack(spacing: 15) {
                                
                                VStack{
                                    
                                    if self.visible {
                                        
                                        TextField("Password", text: self.$pass)
                                        .autocapitalization(.none)
                                    }
                                    else {
                                        
                                        SecureField("Password", text: self.$pass)
                                        .autocapitalization(.none)
                                    }
                                }
                                Button {
                                    self.visible.toggle()
                                } label: {
                                    Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                                        .foregroundColor(self.color)
                                }
                            }
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.pass != "" ? .pink : self.color,lineWidth: 2))
                            .padding(.top, 25)
                            
                            HStack(spacing: 15){
                                
                                VStack{
                                    
                                    if self.revisible {
                                        
                                        TextField("Re-enter", text: self.$repass)
                                        .autocapitalization(.none)
                                    }
                                    else {
                                        
                                        SecureField("Re-enter", text: self.$repass)
                                        .autocapitalization(.none)
                                    }
                                }
                                Button {
                                    self.revisible.toggle()
                                } label: {
                                    Image(systemName: self.revisible ? "eye.slash.fill" : "eye.fill")
                                        .foregroundColor(self.color)
                                }
                            }
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.repass != "" ? .pink : self.color,lineWidth: 2))
                            .padding(.top, 25)
                            
                            Button {
                                self.register()
                            } label: {
                                CustomButtonView(buttonText: "Register", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .pink)
                            }
                            .padding(.top, 25)
                          }
                        }
                        .padding(.horizontal, 25)
                    }
                }
            }
            
            if self.alert{
                ErrorView(alert: self.$alert, error: self.$error)
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    
    func register() {
        
        if self.email != "" {
            
            if self.pass == self.repass {
                
                Auth.auth().createUser(withEmail: self.email, password: self.pass) { (res, err) in
                    Auth.auth().currentUser?.sendEmailVerification { (error) in
                            if err != nil {
                                
                                self.error = err!.localizedDescription
                                self.alert.toggle()
                                return
                            }
                        }
                    if err != nil{
                        
                        self.error = err!.localizedDescription
                        self.alert.toggle()
                        return
                    }
                    //self.error = "We sent a verification e-mail. Check your e-mail address."
                    self.error = "VER"
                    self.alert.toggle()
//                    print("success")
//
//                    UserDefaults.standard.set(true, forKey: "status")
//                    NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
                }
            }
            else {
                
                self.error = "Password mismatch"
                self.alert.toggle()
            }
        }
        else{
            
            self.error = "Please fill all the contents properly"
            self.alert.toggle()
        }
    }
}


struct ErrorView : View {
    
    @State var color = Color.black.opacity(0.7)
    @Binding var alert : Bool
    @Binding var error : String
    
    var body: some View{
        
        VStack {
            
            VStack {
                
                HStack{
                    
                    Text(self.error == "RESET" ? "Message" : self.error == "VER" ? "Message" : "Error")
                    //Text("Message")
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(self.color)
                    
                    Spacer()
                }
                .padding(.horizontal, 25)
                
                Text(self.error == "RESET" ? "Password reset link has been sent successfully" : self.error == "VER" ? "Verification link has been sent to your e-mail address. Please check your e-mail address." : self.error)
                .foregroundColor(self.color)
                .padding(.top)
                .padding(.horizontal, 25)
                
                Button(action: {
                    
                    self.alert.toggle()
                    
                }) {
                    
                    Text(self.error == "RESET" ? "Ok" : self.error == "VER" ? "Ok" : "Cancel")
                        .foregroundColor(.white)
                        .padding(.vertical)
                        .frame(width: UIScreen.main.bounds.width - 120)
                }
                .background(.pink)
                .cornerRadius(10)
                .padding(.top, 25)
                
            }
            .padding(.vertical, 25)
            .frame(width: UIScreen.main.bounds.width - 70, alignment: .center)
            .background(Color.white)
            .cornerRadius(15)
        }
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .center
            )
        .background(Color.black.opacity(0.35).edgesIgnoringSafeArea(.all))
    }
}

