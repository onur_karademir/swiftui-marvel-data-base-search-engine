//
//  ComicViewModel.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import Foundation


struct APIComicResult : Codable {
    var data : APIComicData
}


struct APIComicData : Codable {
    var count : Int
    var results : [Comic]
}


// model //
struct Comic : Identifiable, Codable {
    var id : Int
    var title : String
    var description : String?
    var thumbnail : [String: String]
    var urls : [[String: String]]
    var format : String
    var pageCount : Int
    var diamondCode : String
    var variantDescription : String
}
