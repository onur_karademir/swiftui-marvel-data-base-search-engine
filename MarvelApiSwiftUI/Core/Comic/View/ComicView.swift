//
//  ComicView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct ComicView: View {
    @EnvironmentObject var homeData : HomeViewModel
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                
                if homeData.fatchedComics.isEmpty {
                    ProgressView()
                        .padding(.top, 30)
                } else {
                    VStack (spacing: 15) {
                        MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
                        VStack {
                            CustomTitleView(CustomText: "Marvel's Comics")
                            ForEach(homeData.fatchedComics) { item in
                                ComicRow(character: item)
                            }
                        }
                        // inf. scrool //
                        if homeData.offset == homeData.fatchedComics.count {
                            ProgressView()
                                .padding(.top, 30)
                                .onAppear {
                                    print("fatched new data..")
                                    homeData.fatchComics()
                                }
                        } else {
                            GeometryReader { reader -> Color in
                                
                                let minY = reader.frame(in: .global).minY
                                let heigthC = UIScreen.main.bounds.height / 1.3
                                if !homeData.fatchedComics.isEmpty && minY < heigthC {
                                    DispatchQueue.main.async {
                                        homeData.offset = homeData.fatchedComics.count
                                    }
                                }
                                return Color.clear
                            }
                            .frame(width: 20, height: 20)
                        }
                    }
                    .padding()
                }
            }
            .onAppear{
                if homeData.fatchedComics.isEmpty {
                    homeData.fatchComics()
                }
            }
            .navigationTitle("Marvel's Comics")
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct ComicView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ComicRow: View {
    var character : Comic
    var body: some View {
        HStack (alignment: .center) {
            WebImage(url: extractImage(data: character.thumbnail))
                .resizable()
                .scaledToFill()
                .frame(width: 150, height: 150)
                .background(.red)
                .cornerRadius(8)
            VStack (alignment: .leading, spacing: 10) {
                NavigationLink {
                    ComicDetailView(
                        title: character.title,
                        format: character.format,
                        pageCount: character.pageCount,
                        imgPath: extractImage(data: character.thumbnail),
                        description: character.description ?? "Description not found this character.",
                        diamondCode: character.diamondCode.isEmpty ? "Dimond code found for this comic." : character.diamondCode,
                        variantDescription: character.variantDescription.isEmpty ? "Variant record code found for this comic or character." : character.variantDescription
                    )
                } label: {
                    CharacterTitleView(characterTitle: character.title, font: .title3, color: .black)
                }
                if let description = character.description {
                    CharacterDescriptionView(descTitle: description, font: .caption, color: .gray)
                } else {
                    CharacterDescriptionView(descTitle: "For this comic not found description in Marvel database.", font: .caption, color: .gray)
                }
                
                // links //
                
                HStack (spacing: 10) {
                    ForEach(character.urls, id:\.self) { item in
                        NavigationLink {
                            WebView(url: extractURL(data: item))
                                .navigationTitle(extractURLType(data: item))
                        } label: {
                            Text(extractURLType(data: item))
                                .foregroundColor(.red)
                                .fontWeight(.bold)
                        }

                    }
                }

            }
            Spacer(minLength: 0)
        }
        //.padding(.horizontal)
    }
    
    func extractImage(data: [String : String]) -> URL {
        let path = data["path"] ?? ""
        let ext = data["extension"] ?? ""
        
        return URL(string: "\(path).\(ext)")!
    }
    
    func extractURL(data: [String : String]) -> URL {
        let url = data["url"] ?? ""
        
        return URL(string: url)!
    }
    
    func extractURLType(data: [String : String]) -> String {
        let type = data["type"] ?? ""
        
        return type.capitalized
    }
}
