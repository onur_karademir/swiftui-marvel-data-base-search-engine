//
//  LaunchView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 17.10.2022.
//

import SwiftUI

struct LaunchView: View {
    @State var showSplash : Bool = false
    let pageColor : Color = Color.red
    var body: some View {
        VStack {
            ZStack {
                // Content //
                    pageColor
                    VStack (spacing: 3) {
                        MarvelLogoView(image: "marvel-logo", imageWidth: 280)
                        AppTitleView(title: "DataBase", font: .title, color: .white)
                    }
            }
            .edgesIgnoringSafeArea(.all)
        }
        // i didnt use this logic //
//        .opacity(showSplash ? 0 : 1)
//        .onAppear {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//              withAnimation() {
//                self.showSplash = true
//              }
//            }
//        }

    }
}

struct LaunchView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchView()
    }
}
