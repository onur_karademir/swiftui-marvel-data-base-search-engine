//
//  HomePageView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI

struct HomePageView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView()
    }
}
