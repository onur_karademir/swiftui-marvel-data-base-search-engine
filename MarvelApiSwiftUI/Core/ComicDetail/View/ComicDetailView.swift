//
//  ComicDetailView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 17.10.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct ComicDetailView: View {
    let title : String
    let format : String
    let pageCount : Int
    let imgPath : URL
    let description : String
    let diamondCode : String
    let variantDescription : String
    var body: some View {
        VStack {
            CustomHeaderSetView(systemImage: "arrow.left")
            ScrollView {
                VStack (alignment: .leading, spacing: 15) {
                    WebImage(url: imgPath)
                        .resizable()
                        .scaledToFill()
                        .background(.red)
                        .cornerRadius(20)
                    Text(title)
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                }
                VStack (alignment: .leading, spacing: 8) {
                    Text("Description : ")
                        .font(.title)
                        .fontWeight(.bold)
                    Text(description)
                        .font(.title2)
                    HStack {
                        Text("Page : ")
                            .font(.title)
                            .fontWeight(.bold)
                        Text("\(pageCount)")
                            .font(.title2)
                    }
                    HStack{Spacer()}
                }
                .foregroundColor(.white)
                VStack (alignment: .leading, spacing: 8) {
                    HStack {
                        Text("Format : ")
                            .font(.title)
                            .fontWeight(.bold)
                        Text(format)
                            .font(.title2)
                    }
                    Text("Variant : ")
                        .font(.title)
                        .fontWeight(.bold)
                    Text(variantDescription)
                        .font(.title2)
                    Text("Diamond Code : ")
                        .font(.title)
                        .fontWeight(.bold)
                    Text(diamondCode)
                        .font(.title2)
                    HStack{Spacer()}
                }
                .foregroundColor(.white)
            }
            
        }
        .padding()
        .background(.black)
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    func extractImage(data: [String : String]) -> URL {
        let path = data["path"] ?? ""
        let ext = data["extension"] ?? ""
        
        return URL(string: "\(path).\(ext)")!
    }
}

struct ComicDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ComicDetailView(title: "title", format: "format", pageCount: 7, imgPath: URL(string: "https:")!, description: "description", diamondCode: "diamondCode", variantDescription: "variantDescription")
    }
}
