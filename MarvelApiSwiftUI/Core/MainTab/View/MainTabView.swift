//
//  MainTabView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI

struct MainTabView: View {
    // State Object //
    @StateObject var homeData = HomeViewModel()
    
    var body: some View {
        TabView {
            // Characters View //
            CharactersView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
                .tabItem {
                    Image(systemName: "person.3.fill")
                    Text("Characters")
                }
            // setting environment Object //
            
                .environmentObject(homeData)
            
            // ----- //
            
            // Comics View //
            ComicView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
                .tabItem {
                    Image(systemName: "books.vertical.fill")
                    Text("Comics")
                }
            // setting environment Object //
            
                .environmentObject(homeData)
            
            // ----- //
            ProfileView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Profile")
                }
        }.onAppear {
            let appearance = UITabBarAppearance()
            appearance.backgroundEffect = UIBlurEffect(style: .systemUltraThinMaterial)
            appearance.backgroundColor = UIColor(Color.pink.opacity(0.2))
            
            // Use this appearance when scrolling behind the TabView:
            UITabBar.appearance().standardAppearance = appearance
            // Use this appearance when scrolled all the way up:
            UITabBar.appearance().scrollEdgeAppearance = appearance
        }
        .accentColor(.pink)
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
