//
//  CharactersViewModel.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import Foundation

struct APIResult : Codable {
    var data : APICharacterData
}


struct APICharacterData : Codable {
    var count : Int
    var results : [Character]
}


// model //
struct Character : Identifiable, Codable {
    var id : Int
    var name : String
    var description : String
    var thumbnail : [String: String]
    var urls : [[String: String]]
    var resourceURI : String
}
