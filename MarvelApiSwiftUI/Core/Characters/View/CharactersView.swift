//
//  CharactersView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 15.10.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct CharactersView: View {
    @EnvironmentObject var homeData : HomeViewModel
    var body: some View {
        // navigation View //
        NavigationView {
            
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(spacing: 15) {
                    
                    MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
                    // serach Bar //
                    HStack(spacing: 10) {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                        TextField("Search Character", text: $homeData.searchQuery)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal)
                    .background(.white)
                    // shadow .. //
                    .shadow(color: .pink.opacity(0.3), radius: 5, x: 5, y: 5)
                    .shadow(color: .pink.opacity(0.3), radius: 5, x: -5, y: -5)
                    // --- //
                }
                .padding()
                
                if let characters = homeData.fatchedCharacters {
                    if characters.isEmpty {
                        Text("No Results Found")
                            .padding(.top, 20)
                    } else {
                        // Displaying Results
                        
                        ForEach(characters) { item in
                            CharacterRow(character: item)
                        }
                    }
                } else {
                    // Loading Screen //
                    if homeData.searchQuery != "" {
                        ProgressView()
                            .padding(.top, 20)
                    }
                    VStack {
                        CustomTitleView(CustomText: "Marvel's Comics")
                        ForEach(homeData.fatchedComics.reversed().prefix(10)) { item in
                            ComicRow(character: item)
                        }
                    }
                    .padding(.horizontal)
                }
            }
            .navigationTitle("Marvel")
            .navigationBarTitleDisplayMode(.inline)
            .onAppear{
                if homeData.fatchedComics.isEmpty {
                    homeData.fatchComics()
                }
            }
        }
        
    }
}

struct CharactersView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct CharacterRow: View {
    var character : Character
    var body: some View {
        HStack (alignment: .center) {
            WebImage(url: extractImage(data: character.thumbnail))
                .resizable()
                .scaledToFill()
                .frame(width: 150, height: 150)
                .background(.red)
                .cornerRadius(8)
            VStack (alignment: .leading, spacing: 10) {
                NavigationLink {
                    CharacterDetailView(
                        name: character.name,
                        imgPath: extractImage(data: character.thumbnail),
                        description: character.description.isEmpty ? "Character description not found in Marvel database." : character.description,
                        resourceURI: character.resourceURI.isEmpty ? "Character resourceURI not found in Marvel database." : character.resourceURI
                    )
                } label: {
                    CharacterTitleView(characterTitle: character.name, font: .title3, color: .black)
                }

                CharacterDescriptionView(descTitle: character.description, font: .caption, color: .gray)
                
                // links //
                
                HStack (spacing: 10) {
                    ForEach(character.urls, id:\.self) { item in
                        NavigationLink {
                            WebView(url: extractURL(data: item))
                                .navigationTitle(extractURLType(data: item))
                        } label: {
                            Text(extractURLType(data: item))
                                .foregroundColor(.red)
                                .fontWeight(.bold)
                        }

                    }
                }

            }
            Spacer(minLength: 0)
        }
        .padding(.horizontal)
    }
    
    func extractImage(data: [String : String]) -> URL {
        let path = data["path"] ?? ""
        let ext = data["extension"] ?? ""
        
        return URL(string: "\(path).\(ext)")!
    }
    
    func extractURL(data: [String : String]) -> URL {
        let url = data["url"] ?? ""
        
        return URL(string: url)!
    }
    
    func extractURLType(data: [String : String]) -> String {
        let type = data["type"] ?? ""
        
        return type.capitalized
    }
}
