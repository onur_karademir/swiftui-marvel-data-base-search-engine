//
//  CharacterDescriptionView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 23.10.2022.
//

import SwiftUI

struct CharacterDescriptionView: View {
    let descTitle : String
    let font : Font
    let color : Color
    var body: some View {
        Text(descTitle)
            .font(font)
            .foregroundColor(color)
            .lineLimit(4)
            .multilineTextAlignment(.leading)
    }
}

struct CharacterDescriptionView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterDescriptionView(descTitle: "character.description", font: .caption, color: .gray)
    }
}
