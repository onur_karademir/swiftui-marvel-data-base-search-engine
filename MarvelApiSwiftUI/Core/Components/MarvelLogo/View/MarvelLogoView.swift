//
//  MarvelLogoView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 23.10.2022.
//

import SwiftUI

struct MarvelLogoView: View {
    let image : String
    let imageWidth : CGFloat
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFit()
            .frame(width: imageWidth)
    }
}

struct MarvelLogoView_Previews: PreviewProvider {
    static var previews: some View {
        MarvelLogoView(image: "marvel-logo", imageWidth: 280)
    }
}
