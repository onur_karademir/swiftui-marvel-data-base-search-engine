//
//  LoginSubTitleView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 20.10.2022.
//

import SwiftUI

struct LoginSubTitleView: View {
    let subTitle : String
    let color : Color
    var body: some View {
        Text(subTitle)
            .font(.title)
            .fontWeight(.bold)
            .foregroundColor(color)
            .padding(.top, 35)
    }
}

struct LoginSubTitleView_Previews: PreviewProvider {
    static var previews: some View {
        LoginSubTitleView(subTitle: "Log in to your account", color: .black)
    }
}
