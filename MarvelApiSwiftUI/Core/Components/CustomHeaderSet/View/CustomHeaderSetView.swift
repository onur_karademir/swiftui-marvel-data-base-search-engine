//
//  CustomHeaderSetView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 18.10.2022.
//

import SwiftUI

struct CustomHeaderSetView: View {
    @Environment(\.presentationMode) var presentationMode
    let systemImage : String
    var body: some View {
        HStack {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Image(systemName: systemImage)
                    .font(.title2)
                    .padding(.horizontal, 10)
                    .padding(.vertical, 6)
                    .background(.red)
                    .foregroundColor(.white)
                    .clipShape(Capsule())
            }
            Spacer()
        }
    }
}

struct CustomHeaderSetView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderSetView(systemImage: "arrow.left")
    }
}
