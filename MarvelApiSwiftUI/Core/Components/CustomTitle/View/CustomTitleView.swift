//
//  CustomTitleView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 17.10.2022.
//

import SwiftUI

struct CustomTitleView: View {
    let CustomText : String
    var body: some View {
        Text(CustomText)
            .font(.title3)
            .fontWeight(.semibold)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading)
            .padding(.top)
    }
}

struct CustomTitleView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTitleView(CustomText: "Marvel Comics")
    }
}
