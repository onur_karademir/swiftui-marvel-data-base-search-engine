//
//  MarvelHeaderLogoView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 17.10.2022.
//

import SwiftUI

struct MarvelHeaderLogoView: View {
    let marvelLogo : String
    let marvelSubTitle : String
    var body: some View {
        VStack {
            Image(marvelLogo)
                .resizable()
                .scaledToFit()
                .cornerRadius(10)
            Text(marvelSubTitle)
                .font(.title)
                .foregroundColor(.red)
                .fontWeight(.bold)
                .underline()
                .italic()
        }
        //.padding(.horizontal)
    }
}

struct MarvelHeaderLogoView_Previews: PreviewProvider {
    static var previews: some View {
        MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
    }
}
