//
//  NavBackButtonView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 21.10.2022.
//

import SwiftUI

struct NavBackButtonView: View {
    let image : String
    let font : Font
    let color : Color
    var body: some View {
        Image(systemName: image)
            .font(font)
            .foregroundColor(color)
    }
}

struct NavBackButtonView_Previews: PreviewProvider {
    static var previews: some View {
        NavBackButtonView(image: "chevron.left", font: .title, color: .pink)
    }
}
