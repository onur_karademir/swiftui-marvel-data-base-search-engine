//
//  ProfileUserView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 21.10.2022.
//

import SwiftUI

struct ProfileUserView: View {
    let userEmail : String
    let color : Color
    let image : String
    var body: some View {
        VStack {
            Circle()
                .foregroundColor(color)
                .frame(width: 100, height: 100, alignment: .center)
                .overlay {
                    Image(systemName: image)
                        .font(.system(size: 60))
                        .foregroundColor(.white)
                }
            Text(userEmail)
                .font(.title2)
                .foregroundColor(.black)
                .fontWeight(.bold)
        }
    }
}

struct ProfileUserView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileUserView(userEmail: "asd@.com", color: .pink, image: "person.fill")
    }
}
