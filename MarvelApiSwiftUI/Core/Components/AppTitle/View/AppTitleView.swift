//
//  AppTitleView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 23.10.2022.
//

import SwiftUI

struct AppTitleView: View {
    let title : String
    let font : Font
    let color : Color
    var body: some View {
        Text(title)
            .font(font)
            .foregroundColor(color)
            .fontWeight(.bold)
            .underline()
            .italic()
    }
}

struct AppTitleView_Previews: PreviewProvider {
    static var previews: some View {
        AppTitleView(title: "DataBase", font: .title, color: .white)
    }
}
