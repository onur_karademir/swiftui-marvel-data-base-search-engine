//
//  CharacterTitleView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 23.10.2022.
//

import SwiftUI

struct CharacterTitleView: View {
    let characterTitle : String
    let font : Font
    let color : Color
    var body: some View {
        Text(characterTitle)
            .font(font)
            .fontWeight(.bold)
            .foregroundColor(color)
            .lineLimit(3)
            .multilineTextAlignment(.leading)
    }
}

struct CharacterTitleView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterTitleView(characterTitle: "character.title", font: .title3, color: .black)
    }
}
