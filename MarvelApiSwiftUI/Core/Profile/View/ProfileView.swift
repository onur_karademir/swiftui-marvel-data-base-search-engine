//
//  ProfileView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 20.10.2022.
//

import SwiftUI
import Firebase

struct ProfileView: View {
    
    @State var userEmail : String = ""
    @State var color = Color.black.opacity(0.7)
    
    func getUser () {
        let user = Auth.auth().currentUser
        if let user = user {
            userEmail = user.email ?? "user"
        } else {
            userEmail = ""
        }
    }
    var body: some View {
        NavigationView {
            VStack {
                
                ScrollView {
                    
                    VStack (spacing: 15) {
                        MarvelHeaderLogoView(marvelLogo: "marvel-logo", marvelSubTitle: "DataBase")
                            
                        LoginSubTitleView(subTitle: "User Profile", color: self.color)
                        ProfileUserView(userEmail: userEmail, color: .pink, image: "person.fill")
                        
                        Button {
                            try! Auth.auth().signOut()
                            UserDefaults.standard.set(false, forKey: "status")
                            NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
                        } label: {
                            CustomButtonView(buttonText: "Log out", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .pink)
                        }
                    }
                    .padding()
                }
            }
            .navigationTitle("Profile")
            .navigationBarTitleDisplayMode(.inline)
        }
        .onAppear {
            getUser()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
