//
//  CharacterDetailView.swift
//  MarvelApiSwiftUI
//
//  Created by Onur on 18.10.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct CharacterDetailView: View {
    let name : String
    let imgPath : URL
    let description : String
    let resourceURI : String
    
    var body: some View {
        VStack {
            CustomHeaderSetView(systemImage: "arrow.left")
            ScrollView {
                VStack (alignment: .leading, spacing: 15) {
                    WebImage(url: imgPath)
                        .resizable()
                        .scaledToFill()
                        .background(.red)
                        .cornerRadius(20)
                    Text(name)
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                }
                VStack (alignment: .leading, spacing: 8) {
                    Text("Description : ")
                        .font(.title)
                        .fontWeight(.bold)
                    Text(description)
                        .font(.title2)
                    
                    HStack{Spacer()}
                }
                .foregroundColor(.white)
                VStack (alignment: .leading, spacing: 8) {
                    Text("resourceURI : ")
                        .font(.title)
                        .fontWeight(.bold)
//                    Link(destination: URL(string: resourceURI)!) {
//                        Text(resourceURI)
//                            .font(.title3)
//                    }
                    Text(resourceURI)
                        .font(.title3)
                    HStack{Spacer()}
                }
                .foregroundColor(.white)
            }
            
        }
        .padding()
        .background(.black)
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct CharacterDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterDetailView(name: "name", imgPath: URL(string: "https:")!, description: "description", resourceURI: "resourceURI")
    }
}
